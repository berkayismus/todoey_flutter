class Task {
  final String name;
  bool isDone;

  // task oluşturulduğunda, isDone'ı default olarak false ayarlayalımki, task tamamlanmamış olarak görünsün
  // isDone -> task tile ın sağ tarafındaki checkbox
  Task({this.name, this.isDone = false});

  void toggleDone() {
    isDone = !isDone;
  }
}
