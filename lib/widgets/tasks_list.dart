import 'package:flutter/material.dart';
import 'package:todoey_flutter/models/task.dart';

import 'task_tile.dart';

class TasksList extends StatelessWidget {
  List<Task> tasks = [
    Task(name: 'Buy Some Milk'),
    Task(name: 'Read a book'),
    Task(name: 'Study English'),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: tasks.length,
      itemBuilder: (context, index) {
        return TaskTile(
          taskTitle: tasks[index].name,
          isChecked: tasks[index].isDone,
        );
      },
    );
  }
}
