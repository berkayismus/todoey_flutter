import 'package:flutter/material.dart';

class AddTaskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // child'ını ekranda kaydırabilmek için -> single child scroll view
    return SingleChildScrollView(
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        color: Color(0xFF757575),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 60.0, vertical: 20.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                'Add Task',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.lightBlueAccent,
                  fontSize: 30.0,
                ),
              ),
              TextField(
                controller: TextEditingController(),
                autofocus: true,
              ),
              SizedBox(
                height: 10.0,
              ),
              // elevated buttona genişlik vermek için => constrained box
              ConstrainedBox(
                constraints: BoxConstraints.tightFor(
                    width: double.infinity, height: 50.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.lightBlueAccent,
                    //padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    //textStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {},
                  child: Text('Add'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
